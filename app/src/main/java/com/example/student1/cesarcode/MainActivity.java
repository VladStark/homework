package com.example.student1.cesarcode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView nStroka,oStroka;
    Button bClicker;
    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nStroka = (TextView) findViewById(R.id.new_stroka);
        oStroka = (TextView) findViewById(R.id.old_stroka);
        bClicker = (Button) findViewById(R.id.b_clicker);
        bClicker.setOnClickListener(this);
        StringSplit();

    }

    protected void StringSplit() {

        char [] russiansimvol = {'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
                'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
                'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
                'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',};

        String fisrtName =
                "Ашбт бш ьбёшдшежшё, фжчшё яь рёв дуфвёушёп бу хульи аульбуи! Ао бш вёчуша ьи ыуюуыкьюж!\n" +
                "Хцчлсн ср киъ рциучхд ъ мчъычсцъыкихс шщчлщиххсъыи. Сю кънлч ыщс, с щирьхнныъз ёыч: фнце, цнынщшнфскчъые с лчщмдцз.\n" +
                "Пятрсн эцёцат шьс анш, обсаь яьэюьпьфснай трь обста яшщьыыич ш ыняцщцл эяцгьэна, шьаьюич хынта, рст пи фцптат.\n" +
                "Йцрн ёа ж Java ийоцчжнчйрбту хеёучере цёухпе сшцухе, ёурбэнтцчжу фхузхесс ёа шиердрн цесн цйёд фхн фйхжус лй мефшцпй.\n" +
                "Юклх ыкюьз эыщ лвищ шбфдзы ийзьйщёёвйзыщжвш: лю, жщ дзлзйфю ечэв ыкя ыйюёш ймьщчлкш, в лю, дзлзйфю жвдлз жю вкизехбмюл.";

        String text = "";

        String[] MassiveStrok = fisrtName.split("\n");
        char [] simvol = MassiveStrok[0].toCharArray();
        for(int i = 0;i<simvol.length;i++) {
            for(int j = 0;j < russiansimvol.length-counter;j++) {
                if (simvol[i] == russiansimvol[j]) {
                    simvol[i] = russiansimvol[j + counter];
                    break;
                }
            }
        }
        for (int i =0;i <simvol.length;i++) {
            text +=simvol[i];
        }
        oStroka.setText(MassiveStrok[0]);
        nStroka.setText(text);
        counter++;
    }

    @Override
    public void onClick(View view) {
        StringSplit();
    }
}
